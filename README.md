## qssi-user 10 QKQ1.191215.002 V12.0.4.0.QJPINXM release-keys
- Manufacturer: xiaomi
- Platform: atoll
- Codename: gram
- Brand: POCO
- Flavor: qssi-user
- Release Version: 10
- Id: QKQ1.191215.002
- Incremental: V12.0.4.0.QJPINXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: POCO/gram_in/gram:10/QKQ1.191215.002/V12.0.4.0.QJPINXM:user/release-keys
- OTA version: 
- Branch: qssi-user-10-QKQ1.191215.002-V12.0.4.0.QJPINXM-release-keys
- Repo: poco_gram_dump_29755


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
